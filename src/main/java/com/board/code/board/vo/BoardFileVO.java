package com.board.code.board.vo;

import lombok.Data;

@Data
public class BoardFileVO {

	private int fileId;
	private String originFileName;
	private int boardId;
	private String filePath;
	private String storedFileName;
	private String createDate;
}
