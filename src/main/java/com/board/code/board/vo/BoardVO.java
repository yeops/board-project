package com.board.code.board.vo;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

public class BoardVO {

	@Data
	@Builder // 빌더패턴 등록 annotaion
	public static class Response{
		private List<BoardList> list;
		private int totalSize;
		private int nowPageNumber;
		private String pageHTML;
		
	}
	
	
	@Data
	public static class BoardList{
		
		private int boardId;
		private String boardTitle;
		private String boardOwner;
		private int boardCount;
		private String updateDate;
	}

	@Data
	@Builder
	@NoArgsConstructor
	@AllArgsConstructor
	public static class Detail{
		
		private int boardId;
		private String boardTitle;
		private String boardOwner;
		private int boardCount;
		private String boardContents;
		private List<BoardFileVO> files;
		private String updateDate;
		
//		private String filePath;
//		private String originFileName;
//		private String storedFileName;
	}
	
	@Data
	public static class UpdateRequest{
		
		private int boardId;
		private String boardTitle;
		private String boardContents;
		private MultipartFile[] files;
		// java8 이후부터 사용가능
		// Data보다 정확한 표현
		private LocalDateTime updateDate;
		
	}
	
	@Data
	public static class Request{
		private int boardId;
		private String boardTitle;
		private String boardOwner;
		private String boardContents;
		private MultipartFile[] files;
		
	}
	
}
