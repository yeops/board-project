package com.board.code.board.mapper;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import com.board.code.board.vo.BoardFileVO;
import com.board.code.board.vo.BoardVO;

@Mapper
public interface BoardMapper {

	/**
	 * 게시글 전체 개수 가져오기
	 * @param param
	 * @return
	 * @throws SQLException
	 */
	public int getBoardTotal(Map<String,Object> param) throws SQLException;
	
	/**
	 * 게시글 리스트 가져오기
	 * @param param
	 * @return
	 * @throws SQLException
	 */
	public List<BoardVO.BoardList> getBoardList(Map<String,Object> param) throws SQLException;

	/**
	 * 게시글 쓰기
	 * @param boardRequst
	 * @return
	 * @throws Exception
	 */
	public int writeBoard(BoardVO.Request boardRequst) throws Exception;

	/**
	 * 파일쓰기
	 * @param param
	 * @return
	 * @throws SQLException
	 */
	public int writeFile(Map<String,Object> param) throws SQLException;
	
	/**
	 * 게시글 읽기
	 * @param boardId
	 * @return
	 * @throws SQLException
	 */
	public BoardVO.Detail getBoardDetail(@Param("boardId") int boardId) throws SQLException;
	
	/**
	 * 파일정보 가져오기
	 * @param fileId
	 * @return
	 * @throws SQLException
	 */
	public BoardFileVO getFileInfo(@Param("fileId") int fileId) throws SQLException;
	
}
