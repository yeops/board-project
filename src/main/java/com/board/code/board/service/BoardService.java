package com.board.code.board.service;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import com.board.code.board.mapper.BoardMapper;
import com.board.code.board.vo.BoardFileVO;
import com.board.code.board.vo.BoardVO;
import com.board.code.board.vo.PagingVO;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class BoardService {

	private final BoardMapper mapper;
	private final PagingVO page;
	
	
	public BoardVO.Response getBoardList(Map<String,Object> param) throws Exception{
		int totalSize = 0;
		int nowPageNumber= (int)param.get("nowPageNumber");
		totalSize = mapper.getBoardTotal(param);
		
		List<BoardVO.BoardList> list = new ArrayList<>();
		//페이징 처리를 위한 데이터 삽입
		page.dataInit(nowPageNumber, totalSize);
		
		if(totalSize>0) {
			param.put("start", this.page.getBeginPage());
			param.put("end", this.page.getEndPage());
			
			list = mapper.getBoardList(param);
		}
		
		//빌더패턴을 이용하여 객체를 new 없이 만들고 바로리턴
		// 사용하려면 no와 all argscostruct?선언해야함?안해도 되는데?
		return BoardVO.Response
				       .builder()
				       .list(list)
				       .totalSize(totalSize)
				       .nowPageNumber(nowPageNumber)
				       .pageHTML(this.page.pageHTML())
				       .build();
	}
	
	//트랜잭션 적용
	@Transactional
	public int writeBoard(BoardVO.Request boardRequest) throws Exception {
		
		boardRequest.setBoardOwner("관리자");
		
//		BoardVO.Detail detail = BoardVO.Detail
//				                       .builder()
//				                       .boardTitle(boardRequest.getBoardTitle())
//				                       .boardContents(boardRequest.getBoardContents())
//				                       .boardOwner("관리자")
//				                       .build();
		
		//insert 부터 해야함
		// mybatis insert시 auto_increment 가져오기
		int result = mapper.writeBoard(boardRequest);
		
		if(result > 0) {
		
			if(boardRequest.getFiles() != null) {
				this.uploadFiles(boardRequest.getBoardId(), boardRequest.getFiles());
			}
		}
		return result;
	}
	
	private int uploadFiles(int boardId, MultipartFile[] files) throws Exception{
		
		List<BoardFileVO> fileList = new ArrayList<>();
		
		for(MultipartFile file: files) {
			// 객체가 비어있지 않다면
			if(file != null && !file.isEmpty()) {
				
				//파일 원본 이름 가져오기
				String originFileName = file.getOriginalFilename();
				
				//확장자 자겨오기
				String ext = originFileName.substring(originFileName.lastIndexOf(".")+1);
				
				//중복되지 않는 이름 만들기
				// uuid는 네트워크 상에서 객체식별자(식별할떄 사용)
				// 너무 길어서 - 지우고 12자로 자름
				String uuid = UUID.randomUUID().toString().replaceAll("-","").substring(0,12);
			
				String storedFileName = uuid +"." + ext;
			
				String filePath = "K:\\my_workspace\\sts_workspace\\boardDownload\\";
				// 파일을 포함한 전체 경로
				String fullPath = filePath + storedFileName;
				
				//파일객체 만들기
				File newFile = new File(fullPath);
				
				//경로가 없다면 만들자!!
				//getParentFile은 폴더임 여기서는 다운로드 (파일의 상위경로)
				// --> 파일이 속한 경로를 객체로 반환 (filepath)
				if(!newFile.getParentFile().exists()) {
					// dir은 만들때(download)
					// 만약 상위폴더가 없어 그러면 안만든다.
					// 생성해야할 폴더의 상위가 없을경우 못만듬
					// dirs는 없으면 상위까지 만들어버림
					
					//생성할 폴더의 상위폴더가 없을 경우도, 같이 생성해준다.
					newFile.getParentFile().mkdirs();
				}
				
				newFile.createNewFile(); //빈 파일 생성
				//기존파일내용을 빈 파일로 전송
				file.transferTo(newFile);
				
				BoardFileVO fileVO = new BoardFileVO();
				fileVO.setBoardId(boardId);
				fileVO.setFilePath(filePath);
				fileVO.setOriginFileName(originFileName);
				fileVO.setStoredFileName(storedFileName);
				fileList.add(fileVO);
			}
		}
		
		Map<String,Object> param = new HashMap<>();
		param.put("fileList", fileList);
		return mapper.writeFile(param);
	}
	
	/**
	 * 해당 게시글 정보 가져오기 (디테일)
	 * @param boardId
	 * @return
	 * @throws Exception
	 */
	public BoardVO.Detail getBoardDetail(int boardId) throws Exception{
		return mapper.getBoardDetail(boardId);
	}
	
	
	/**
	 * 파일 정보 가져오기 
	 * @param fileId
	 * @return
	 * @throws Exception
	 */
	public BoardFileVO getFileInfo(@Param("fileId") int fileId) throws Exception{
		
		return mapper.getFileInfo(fileId);
	}
	
}
