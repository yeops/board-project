package com.board.code.board.controller;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.board.code.board.service.BoardService;
import com.board.code.board.vo.BoardVO;

import lombok.RequiredArgsConstructor;

@Controller
@RequiredArgsConstructor
@RequestMapping("/board")
public class BoardController {

	private final BoardService boardService;
	
	@GetMapping("/boardlist")
	public ModelAndView getBoardList(@RequestParam(value="nowPageNumber", defaultValue = "0") int nowPageNumber) {
		
		ModelAndView view = new ModelAndView();
		BoardVO.Response resp = null;
		Map<String,Object> param = new HashMap<>();
		param.put("nowPageNumber", nowPageNumber);
		try {
			//ajax라면 이거 리스폰스 그냥 던저도 된다? 그냥 던저보자 ㅋ
			resp = boardService.getBoardList(param);
			view.addObject("data", resp);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		view.setViewName("views/board/boardlist");
		return view;
	}
	
	@GetMapping("/add/view")
	public ModelAndView boardWriteForm(@RequestParam(value="nowPageNumber",defaultValue = "0") int nowPageNumber) {
		ModelAndView view = new ModelAndView();
		
		view.addObject("nowPageNumber", nowPageNumber);
		view.setViewName("views/board/boardWrite");
		
		return view;
	}
	
	@PostMapping("/add")
	@ResponseBody
	public Map<String,Object> writeBoard(@ModelAttribute BoardVO.Request boardRequest){
		Map<String,Object> resultMap = new HashMap<>();
		
		try {
			int result = boardService.writeBoard(boardRequest);
			
			if(result > 0) {
				resultMap.put("resultCode", 200);
			}else {
				throw new Exception("insert Errot");
			}
		} catch (Exception e) {
			resultMap.put("resultCode", 500);
			e.printStackTrace();
		}
		return resultMap;
	}
	
	@GetMapping("/detail/view")
	public ModelAndView boardDetailForm(@RequestParam(value="nowPageNumber", defaultValue="0") int nowPageNumber,
			                            @RequestParam(value="boardId") int boardId) throws Exception{
		
		ModelAndView view = new ModelAndView();
		//현재 페이지 번호 저장
		view.addObject("nowPageNumber", nowPageNumber);
		view.setViewName("views/board/boardDetail");
		
		
		try {
			//게시글 상세보기
			BoardVO.Detail detail = boardService.getBoardDetail(boardId);
			//게시글 상세정보 view에 넘김
			view.addObject("detail", detail);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return view;
	}
}
