package com.board.code.board.controller;

import java.io.File;
import java.net.URLEncoder;
import java.nio.file.Files;
import java.nio.file.Paths;

import org.springframework.core.io.UrlResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.board.code.board.service.BoardService;
import com.board.code.board.vo.BoardFileVO;

import lombok.RequiredArgsConstructor;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api") //기본경로 매핑
public class BoardApiController {

	private final BoardService service;
	private String filePath = "K:\\my_workspace\\sts_workspace\\boardDownload\\";

	
	@GetMapping("/board/fileDown") 
	public ResponseEntity<UrlResource> downBoardFile(@RequestParam("fileId") int fileId){
		
		
		String originFileName = null;
	    String storedFileName = null;
	    
	    //파일 다운로드시 필요
	    // 우리가 전송하는 헤더는 리스폰스엔티티는 리스폰스에 관련된 http객체를 가지고있응ㅁ
	    // http객체는 헤더,바디,스테이터스가 있음
	    // 헤더에는 전송하는 아이(리스폰스)가 어떤 타입이고 어떤 형식인지 저장하는 컨텐트 타입이 있음 -- 이걸 파일다운로드 타입으로 바꾸면되고
	    // 스테이터스는 ㅜㅇ리가 전송하는게 정상적이다 라고 해준다. 그러면 브라우전ㄴ 우리가 전송하는 이것이 전송이라고 알거임
		HttpHeaders header = new HttpHeaders();
		
		
		// 전송할 파일객체를 담으려고 만든거임
		UrlResource res = null;
		
		try {
			BoardFileVO fileVO = service.getFileInfo(fileId);
			
			if(fileVO != null) {
				
				originFileName = fileVO.getOriginFileName();
				storedFileName = fileVO.getStoredFileName();
				
				//stored는 물리적으로 등록된 이름이므로 그냥 쓰면됨?
				String fullPath = filePath + storedFileName;
				
				//파일 객체에 집어넣음
				File file = new File(fullPath);
				
				if(!file.exists()) {
					throw new Exception("파일이 존재하지 않습니다");
				}
				
				// 파일에 mineType이라는게 있은데
				// 우리가 인터넷에서 파일을 전송할때 이 파일이 어떠한 형태를 가지고 있는지 알려주는것 
				// 마인타입을 구할수 있는 형식
				// Files 객체는 io객체이고  ㅈ바 1.7에서 나왔다.
				
				//파일을 전송할때 필요한 파일의 확장 타입(구조 객체?)
				String mineType = Files.probeContentType(Paths.get(file.getAbsolutePath()));
				
				
				
				
				// 파일에 있는 전송타입을 뽑는데 없을 수도 있음
				// 없으면 일반 이진타입으로?
				// 보통 노래나 동영상파일은 있는데 텍스트 파일은 없다??
				if(mineType == null) {
					//일반 이진파일 타입(텍스트?) 없는경우도 있다
					mineType = "octet-stream"; // 일반 파일 일반 데이터 파일이다 라는 뜻
				}
				
				//전송할 파일을 객체에 담는다.
				//파일의 touri해서 파일의 상대경로!로서 파일을 찾는데, 파일을 url리소스에 담는다.
				res = new UrlResource(file.toURI());
				
				//한글깨짐 방지 정책
				//인코딩하면 +기호가생김 (%20)으로
				// +기호는 경로에서 오류발생 떄문에 encode 후  +기호가 바이트 코드인 %20으로 변경
				String encodedName = URLEncoder.encode(originFileName,"UTF-8").replace("+", "%20");
				
				//http 헤더 셋팅
				// 파일 다운로드 옵션과 다운로드 할떄의 파일 이름 지정
				
				//컨텐츠 디스포지션은 다운로드 타입임(첨부파일을 넣는다)
				// 앞에있는 enname은 첨부파일 이고 뒤에있는건 영문?다운로드 보여주는거?디스플레이 처음에는 
				// '' 빈공백 넣어야함 왜 그런지는 얘도 모른다고함 
				header.set("Content-Disposition", "attachment;filename=" + encodedName + ";filename*=UTF-8''" + encodedName);
				// 캐시 사용 안함
				header.setCacheControl("no-cache"); 
				// 다운로드할 파일의 타입 
				header.setContentType(MediaType.parseMediaType(mineType));
				
				
			}else {
//				throw new Exception("파일이 존재하지 않습니다.");
				throw new Exception("SQL 오류");
			}
			
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return new ResponseEntity<UrlResource>(res,header,HttpStatus.OK);
	}
}
